from django.forms import CharField, ModelForm, Textarea

from blog.models.Article import Article


class ArticleForm(ModelForm):
    title = CharField(max_length=80)
    content = CharField(max_length=4096, widget=Textarea)

    class Meta:
        model = Article
        fields = ('title', 'content')
