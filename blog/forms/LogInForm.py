from django import forms


class LogInForm(forms.Form):
    username = forms.CharField(
        min_length=3,
        max_length=320,
        required=True
    )

    password = forms.CharField(
        min_length=5,
        widget=forms.PasswordInput,
        required=True
    )

