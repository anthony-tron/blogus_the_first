from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.urls import reverse_lazy
from django.views.generic import DeleteView
from django.utils.translation import gettext_lazy as _

from blog.models.Article import Article


class ArticleDeleteView(LoginRequiredMixin, SuccessMessageMixin, DeleteView):
    template_name = 'article_delete.html'
    model = Article
    success_url = reverse_lazy('article_list')
    success_message = _('Successfully deleted the article.')
