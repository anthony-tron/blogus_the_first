from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.urls import reverse_lazy
from django.utils.translation import gettext_lazy as _
from extra_views import CreateWithInlinesView

from blog.forms.ArticleForm import ArticleForm
from blog.formsets.ArticleCategoryFormSet import ArticleCategoryFormSet
from blog.formsets.ArticleTagFormSet import ArticleTagFormSet
from blog.models.Article import Article
from blog.models.ArticleUser import ArticleUser


class ArticleCreateView(LoginRequiredMixin, SuccessMessageMixin, CreateWithInlinesView):
    template_name = 'article_create.html'
    model = Article
    form_class = ArticleForm
    inlines = (ArticleTagFormSet, ArticleCategoryFormSet, )
    success_url = reverse_lazy('article_list')
    success_message = _('Successfully published an article.')

    def form_valid(self, form):
        article = form.save()
        article_user = ArticleUser(article=article, user=self.request.user)
        article_user.save()
        return super().form_valid(form)
