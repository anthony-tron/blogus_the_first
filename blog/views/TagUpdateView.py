from django.urls import reverse_lazy
from django.views.generic import UpdateView
from django.utils.translation import gettext_lazy as _

from blog.models.Tag import Tag


class TagUpdateView(UpdateView):
    model = Tag
    fields = ('name', )
    template_name = 'tag_create.html'
    success_url = reverse_lazy('tag_list')
    success_message = _('Tag successfully updated.')


