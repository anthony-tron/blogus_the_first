from django.views.generic import ListView

from blog.models.Tag import Tag


class TagListView(ListView):
    template_name = 'tag_list.html'
    model = Tag
    paginate_by = 6
