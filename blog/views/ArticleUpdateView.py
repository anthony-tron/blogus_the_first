from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.urls import reverse_lazy
from django.views.generic import UpdateView
from django.utils.translation import gettext_lazy as _
from extra_views import UpdateWithInlinesView

from blog.forms.ArticleForm import ArticleForm
from blog.formsets.ArticleCategoryFormSet import ArticleCategoryFormSet
from blog.formsets.ArticleTagFormSet import ArticleTagFormSet
from blog.models.Article import Article


class ArticleUpdateView(LoginRequiredMixin, SuccessMessageMixin, UpdateWithInlinesView):
    template_name = 'article_create.html'
    model = Article
    form_class = ArticleForm
    inlines = (ArticleTagFormSet, ArticleCategoryFormSet, )
    success_url = reverse_lazy('article_list')
    success_message = _('Successfully updated the article.')
