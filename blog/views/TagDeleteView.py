from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.urls import reverse_lazy
from django.views.generic import DeleteView
from django.utils.translation import gettext_lazy as _

from blog.models.Tag import Tag


class TagDeleteView(LoginRequiredMixin, SuccessMessageMixin, DeleteView):
    template_name = 'tag_delete.html'
    model = Tag
    success_url = reverse_lazy('tag_list')
    success_message = _('Tag successfully deleted.')
