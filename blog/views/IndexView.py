from django.views.generic import RedirectView


class IndexView(RedirectView):
    pattern_name = 'article_list'
