from django.contrib.auth.models import User
from markdown import markdown
from django.views.generic import DetailView

from blog.models.Article import Article
from blog.models.Category import Category
from blog.models.Tag import Tag


class ArticleReadView(DetailView):
    template_name = 'article_read.html'
    model = Article

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context['safe_html_content'] = markdown(self.object.content)

        context['users'] = User.objects.all().filter(
            articleuser__article__pk=self.object.pk
        )

        context['categories'] = Category.objects.all().filter(
            articlecategory__article__pk=self.object.pk
        )

        context['tags'] = Tag.objects.all().filter(
            articletag__article__pk=self.object.pk
        )

        return context
