from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.urls import reverse_lazy
from django.views.generic import UpdateView
from django.utils.translation import gettext_lazy as _

from blog.models.Category import Category


class CategoryUpdateView(LoginRequiredMixin, SuccessMessageMixin, UpdateView):
    model = Category
    fields = ('name', )
    template_name = 'category_create.html'
    success_url = reverse_lazy('category_list')
    success_message = _('Category updated successfully.')
