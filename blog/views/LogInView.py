from django.contrib import messages
from django.contrib.auth import authenticate, login
from django.urls import reverse_lazy
from django.views.generic import FormView
from django.utils.translation import gettext_lazy as _

from blog.forms.LogInForm import LogInForm


class LogInView(FormView):
    template_name = 'login.html'
    form_class = LogInForm
    success_url = reverse_lazy('index')

    def form_valid(self, form):
        username = form.cleaned_data['username']
        password = form.cleaned_data['password']
        user = authenticate(username=username, password=password)

        if user is not None:
            login(self.request, user)
            messages.add_message(
                self.request,
                messages.INFO,
                _('Welcome back %(user)s!') % {'user': user.username}
            )
            return super().form_valid(form)

        # TODO i18n
        form.add_error(
            None,
            _('Invalid credentials.')
        )

        return self.form_invalid(form)
