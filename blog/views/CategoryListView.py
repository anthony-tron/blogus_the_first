from django.views.generic import ListView

from blog.models.Category import Category


class CategoryListView(ListView):
    model = Category
    template_name = 'category_list.html'
    paginate_by = 6

