from django.views.generic import ListView

from blog.models.Article import Article

limit = 3


class BestArticlesView(ListView):
    http_method_names = ['get']
    template_name = 'best_articles.html'
    queryset = Article.objects.all()[:limit]
