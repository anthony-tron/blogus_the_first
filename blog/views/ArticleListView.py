from django.views.generic import ListView

from blog.models.Article import Article


class ArticleListView(ListView):
    template_name = 'article_list.html'
    paginate_by = 5

    def get_queryset(self):
        tag = self.request.GET.get('tag')
        category = self.request.GET.get('category')

        object_list = Article.objects.all()

        if tag:
            object_list = object_list.filter(
                articletag__tag__name__icontains=tag
            )

        if category:
            object_list = object_list.filter(
                articlecategory__category__name__icontains=category
            )

        return object_list
