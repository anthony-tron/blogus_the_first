from extra_views import InlineFormSetFactory

from blog.models.ArticleTag import ArticleTag


class ArticleTagFormSet(InlineFormSetFactory):
    model = ArticleTag
    fields = ('tag', )
