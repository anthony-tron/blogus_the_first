from extra_views import InlineFormSetFactory

from blog.models.ArticleCategory import ArticleCategory


class ArticleCategoryFormSet(InlineFormSetFactory):
    model = ArticleCategory
    fields = ('category', )
