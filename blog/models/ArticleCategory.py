from django.db.models import Model, ForeignKey, CASCADE

from blog.models.Article import Article
from blog.models.Category import Category


class ArticleCategory(Model):
    article = ForeignKey(Article, on_delete=CASCADE)
    category = ForeignKey(Category, on_delete=CASCADE)

    def __str__(self):
        return f'#{self.id} {self.article}, {self.category}'
