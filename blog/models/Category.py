from django.db.models import Model, CharField


class Category(Model):
    name = CharField(max_length=80)

    def __str__(self):
        return f'{self.name}'
