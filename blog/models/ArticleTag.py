from django.db.models import Model, ForeignKey, CASCADE

from blog.models.Article import Article
from blog.models.Tag import Tag


class ArticleTag(Model):
    article = ForeignKey(Article, on_delete=CASCADE)
    tag = ForeignKey(Tag, on_delete=CASCADE)

    def __str__(self):
        return f'{self.article}, {self.tag}'
