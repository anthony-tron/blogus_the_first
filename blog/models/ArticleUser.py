from django.contrib.auth.models import User
from django.db.models import Model, CASCADE, ForeignKey

from blog.models.Article import Article


class ArticleUser(Model):
    article = ForeignKey(Article, on_delete=CASCADE)
    user = ForeignKey(User, on_delete=CASCADE)

    def __str__(self):
        return f'{self.article}, {self.user}'
