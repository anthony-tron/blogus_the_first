from django.db.models import Model, CharField


class Article(Model):
    title = CharField(max_length=80)
    content = CharField(max_length=4096)

    def __str__(self):
        return f'{self.title}'
