from django.contrib import admin

from blog.models.Article import Article
from blog.models.ArticleCategory import ArticleCategory
from blog.models.ArticleTag import ArticleTag
from blog.models.ArticleUser import ArticleUser
from blog.models.Category import Category
from blog.models.Tag import Tag


class ArticleCategoryInline(admin.TabularInline):
    model = ArticleCategory
    max_num = 1


class ArticleTagInline(admin.TabularInline):
    model = ArticleTag
    max_num = 3


class ArticleUserInline(admin.TabularInline):
    model = ArticleUser


class ArticleAdmin(admin.ModelAdmin):
    inlines = (
        ArticleCategoryInline,
        ArticleTagInline,
        ArticleUserInline,
    )


admin.site.register(Article, ArticleAdmin)
admin.site.register(Category)
admin.site.register(Tag)
admin.site.register(ArticleCategory)
admin.site.register(ArticleTag)
admin.site.register(ArticleUser)
