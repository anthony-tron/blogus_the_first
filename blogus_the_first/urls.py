"""blogus_the_first URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf.urls.i18n import i18n_patterns
from django.contrib import admin
from django.urls import path

from blog.views.ArticleDeleteView import ArticleDeleteView
from blog.views.CategoryCreateView import CategoryCreateView
from blog.views.CategoryDeleteView import CategoryDeleteView
from blog.views.CategoryListView import CategoryListView
from blog.views.CategoryUpdateView import CategoryUpdateView
from blog.views.IndexView import IndexView
from blog.views.LogInView import LogInView
from blog.views.ArticleCreateView import ArticleCreateView
from blog.views.ArticleListView import ArticleListView
from blog.views.ArticleReadView import ArticleReadView
from blog.views.ArticleUpdateView import ArticleUpdateView
from blog.views.BestArticlesView import BestArticlesView
from blog.views.LogOutView import LogOutView
from blog.views.SignUpView import SignUpView
from blog.views.TagCreateView import TagCreateView
from blog.views.TagDeleteView import TagDeleteView
from blog.views.TagListView import TagListView
from blog.views.TagUpdateView import TagUpdateView

urlpatterns = i18n_patterns(
    path('admin/', admin.site.urls),
    path('', IndexView.as_view(), name='index'),
    path('login', LogInView.as_view(), name='login'),
    path('logout', LogOutView.as_view(), name='logout'),
    path('signup', SignUpView.as_view(), name='signup'),
    path('articles', ArticleListView.as_view(), name='article_list'),
    path('article/<int:pk>', ArticleReadView.as_view(), name='article_read'),
    path('article/create', ArticleCreateView.as_view(), name='article_create'),
    path('article/update/<int:pk>', ArticleUpdateView.as_view(), name='article_update'),
    path('article/delete/<int:pk>', ArticleDeleteView.as_view(), name='article_delete'),
    path('tags', TagListView.as_view(), name='tag_list'),
    path('tag/create', TagCreateView.as_view(), name='tag_create'),
    path('tag/update/<int:pk>', TagUpdateView.as_view(), name='tag_update'),
    path('tag/delete/<int:pk>', TagDeleteView.as_view(), name='tag_delete'),
    path('categories', CategoryListView.as_view(), name='category_list'),
    path('category/create', CategoryCreateView.as_view(), name='category_create'),
    path('category/update/<int:pk>', CategoryUpdateView.as_view(), name='category_update'),
    path('category/delete/<int:pk>', CategoryDeleteView.as_view(), name='category_delete'),
)
