# blogus-the-first

`blogus-the-first` is a free django app that you can deploy to make your own blog.

`blogus-the-first` supports:
- publishing article with `markdown` syntax only
- adding tags and categories to a blog
- editing content such as articles' content, and tags' names

Example: https://anthonytron.pythonanywhere.com

## Get started

- `DEBUG=1 SECRET_KEY=abcdef python manage.py runserver 80`
> If you wish to deactivate debug mode, omit the `DEBUG=1`. **Do not run it with `DEBUG=False`** as it will actually enable debug mode!

## Deploy

- `python manage.py migrate`
- `python manage.py collectstatic`
- `python manage.py compilemessages`
- `SECRET_KEY=abcdef python manage.py runserver 80`
> Replace `abcdef` by a safe and private value.
